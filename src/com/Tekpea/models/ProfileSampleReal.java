package com.Tekpea.models;

import java.util.Date;

public class ProfileSampleReal {

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProfile_param_idx() {
		return profile_param_idx;
	}

	public void setProfile_param_idx(int profile_param_idx) {
		this.profile_param_idx = profile_param_idx;
	}

	public int getDevice_inst_idx() {
		return device_inst_idx;
	}

	public void setDevice_inst_idx(int device_inst_idx) {
		this.device_inst_idx = device_inst_idx;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	private int id;
	private int profile_param_idx;
	private int device_inst_idx;
	private double value;
	private Date ts;
}
