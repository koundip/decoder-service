package com.Tekpea.models;

public class ProfileParam {

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getObis() {
		return obis;
	}

	public void setObis(String obis) {
		this.obis = obis;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIc() {
		return ic;
	}

	public void setIc(int ic) {
		this.ic = ic;
	}

	public int getAi() {
		return ai;
	}

	public void setAi(int ai) {
		this.ai = ai;
	}

	private int id;
	private String obis;
	private String name;
	private int ic;
	private int ai;
}
