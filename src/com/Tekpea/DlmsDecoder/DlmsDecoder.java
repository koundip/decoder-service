package com.Tekpea.DlmsDecoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import com.Tekpea.Decoder.BaseDecoder;
import com.Tekpea.Decoder.BaseDecoderInterface;
import com.Tekpea.models.ProfileParam;
import com.Tekpea.models.ProfileSampleRaw;


public class DlmsDecoder extends BaseDecoder implements BaseDecoderInterface {

	private byte[] GetProfileDefValue(ProfileSampleRaw sample) {
		// TODO Auto-generated method stub
		try {
			int profile_idx = sample.getProfile_param_idx();
			int profileDef_idx = this.GetProfileDefIndex(profile_idx);

			if (profileDef_idx == -1) {
				System.out.println("Definition for profile doesnt exsist in the table");
				// TODO add profile to the definition
				return null;
			}

			// we have profile and its def get the profileDef buffer from profileSampleRaw
			String QueryStr = new String("from com.Tekpea.models.ProfileSampleRaw WHERE profile_param_idx=:param_idx AND device_inst_idx=:device_idx");
			
			Session sess = this.getSession();
			sess.beginTransaction();
			Query<ProfileSampleRaw> query = sess.createQuery(QueryStr,ProfileSampleRaw.class);
			query.setParameter("param_idx", profileDef_idx);
			query.setParameter("device_idx", sample.getDevice_inst_idx());
			List<ProfileSampleRaw> def = query.getResultList();
			sess.getTransaction().commit();

			if (def.size() < 1) {
				System.out.println("Static param Value for the ProfileDefinition doesnt exsist!!");
				return null;
			}

			// Ideally you will have only one entry, even if you have multiple entries its
			// ok?? maybe not sure
			return def.get(0).getValue();
		} catch (Exception e) {
			System.out.println("Error occured while fetching profileDefinition buffer");
			e.printStackTrace();
			return null;
		}
	}

	private int GetProfileDefIndex(int param_idx) {
		try {
			Session sess = this.getSession();
			sess.beginTransaction();

			ProfileParam param = sess.get(ProfileParam.class, param_idx);
			
			if(param == null) {
				System.out.println("Profile param with given idx not found :" + param_idx);
				return -1;
			}

			String QueryStr = new String("FROM com.Tekpea.models.ProfileParam WHERE obis=:obis AND ic=:ic AND ai= 3");
			System.out.println("Querying data base : " + QueryStr);
			
			Query<ProfileParam> query = sess.createQuery(QueryStr,ProfileParam.class);
			query.setParameter("obis", param.getObis());
			query.setParameter("ic", param.getIc());

			List<ProfileParam> def = query.getResultList();
			sess.getTransaction().commit();

			if (def.isEmpty()) {
				System.out.println("No matching Profile Definition found!");
				return -1;
			}

			if (def.size() != 1) {
				System.out.println("resultset size is > 1 shouldnt happen!!");
				return -1;
			}
			return def.get(0).getId();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error occured while finding the idx of profile definition");
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public void Decode(ProfileSampleRaw profileSampleRaw){
		// TODO Auto-generated method stub
		byte[] defBuffer = this.GetProfileDefValue(profileSampleRaw);

		if (defBuffer == null) {
			System.out.println("No profile Definition Value found");
			return;
		}

		byte[] profile = profileSampleRaw.getValue();
		InputStream profileIs = new ByteArrayInputStream(profile);
		InputStream profileDefIs = new ByteArrayInputStream(defBuffer);

		try {
			DLMSProfileDefinition dataDef[] = DLMSProfileDefinition.buildArrayFromStream(profileDefIs);
			DLMSProfileValueArray dataVal = new DLMSProfileValueArray(dataDef);

			int b = profileIs.read();
			if (b == DLMSDecoderUtils.CosemDataType_Struct) {
				DLMSProfileValue val = new DLMSProfileValue(dataDef, profileIs);
				dataVal.append(val);
			} else {
				dataVal.appendFromStream(profileIs);
			}
			// get all the scaler values!!
			ScalerValue[] scalerValueArray = new ScalerValue[dataDef.length];
			for (int i=0; i< dataDef.length;i++) {
				scalerValueArray[i] = GetScalerValue(profileSampleRaw,dataDef[i]); 
			}
			// Dump the data values formatted accordingly
			System.out.println("Value file contains: " + dataVal.getSize() + " records:");
			for (int i = 0; i < dataVal.getSize(); ++i) {
				System.out.println("Sample #" + i + ":");
				System.out.println(dataVal.getItem(i).toJSON("\t", scalerValueArray));
			}
		} catch (IOException e) {
			System.err.println("Error reading file: " + e.toString());
			e.printStackTrace();
		} catch (Error e) {
			System.err.println("Parse error: " + e.toString());
			e.printStackTrace();
		}
	}

	private ScalerValue GetScalerValue(ProfileSampleRaw profileSampleRaw,DLMSProfileDefinition dlmsProfileDefinition) {
		byte[] obis = dlmsProfileDefinition.m_obis;
		int param_idx;
		Session sess;
		String query;
		String obisString = String.format("%d.%d.%d.%d.%d.%d", obis[0],obis[1],obis[2],obis[3],obis[4],obis[5]);
		System.out.println(obisString);
		
		//use this obis code and get the scaler for this!!
		sess = this.getSession();
		sess.beginTransaction();
		query = new String("FROM com.Tekpea.models.ProfileParam WHERE obis="+obisString);
		List<ProfileParam> qlist = sess.createQuery(query, ProfileParam.class).getResultList();
		sess.getTransaction().commit();

		//ideally there should be only one entry fot this in the profile_sample_real
		if(qlist.size() < 1) {
			System.out.println("no scaler found for obis code :"+obisString);
			return null;
		}

		ProfileParam param = qlist.get(0);
		param_idx = param.getId();

		query = new String("FROM com.Tekpea.models.ProfileSampleReal WHERE profile_param_idx="+param_idx+" AND device_inst_idx="+profileSampleRaw.getDevice_inst_idx());
		List<ProfileSampleRaw> slist = sess.createQuery(query, ProfileSampleRaw.class).getResultList();
		sess.getTransaction().commit();

		if(slist.size() <1) {
			System.out.println("no profile param found with obis code "+obisString);
			return null;
		}

		//we have a static param with the given obis code add it to the scaler array
		//Decoder code expects a array of two numbers as scaler!!
		ProfileSampleRaw scalerValue = slist.get(0);
		ScalerValue ret = new ScalerValue(scalerValue,dlmsProfileDefinition);

		return ret;
	}

	@Override
	public void start() throws InterruptedException {
		Session sess = this.getSession();
		while (true) {

			List<ProfileSampleRaw> list = null;

			try {
				sess.beginTransaction();
				list = sess.createQuery("FROM com.Tekpea.models.ProfileSampleRaw WHERE decoded=0",ProfileSampleRaw.class)
						.getResultList();
				sess.getTransaction().commit();
			} catch (HibernateException e1) {
				System.out.println("Hibernate exception while performing begin transation in main loop!");
				e1.printStackTrace();
			}

			if(list == null) {
				Thread.sleep(2000);
				continue;
			}

			if (!list.isEmpty()) {
				System.out.println("Found " + list.size() + "samples to be decoded");
			} else {
				System.out.println("No entry found with decoded flag 0");
				Thread.sleep(2000);
				continue;
			}

			for (ProfileSampleRaw profileSampleRaw : list) {
				System.out.println("Decoding new undecoded DLMS Sample");
				this.Decode(profileSampleRaw);
			}
		}
		// TODO this.factory.close();
	}
}
