package com.Tekpea.DlmsDecoder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.Tekpea.models.ProfileSampleRaw;

public class ScalerValue{

	public ScalerValue(ProfileSampleRaw scaler, DLMSProfileDefinition def) throws IOException {
		// TODO Auto-generated constructor stub
		this.m_ic = def.m_ai;
		this.m_di = def.m_di;
		this.m_ic = def.m_ic;
		this.m_obis = def.m_obis.clone();
		this.m_value = DecodeScaler(scaler);
	}
	private Number[] DecodeScaler(ProfileSampleRaw scaler) throws IOException {
		// Scaler basically has two values, a scaling factor and a unit
		InputStream is = new ByteArrayInputStream(scaler.getValue());
		Object ret = DLMSDecoderUtils.decodeStream(is);
		
		//ret should return an array of two numbers
		return (Number[]) ret;
	}
	public Number[] getM_value() {
		return m_value;
	}

	public int m_ic    	 = 0;			// InterfaceClass
	public byte m_obis[] = {0, 0, 0, 0, 0, 0};	// OBIS Code
	public int m_ai      = 0;			// AttributeIndex
	public int m_di      = 0;			// Data Index
	Number[] m_value;
}
