/*
 *  (c) Copyright, Tekpea Inc., 2014 All rights reserved.
 *
 *  No duplications, whole or partial, manual or electronic, may be made
 *  without express written permission.  Any such copies, or
 *  revisions thereof, must display this notice unaltered.
 *  This code contains trade secrets of Tekpea, Inc.
 *
 */


package com.Tekpea.DlmsDecoder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;

public class DLMSProfileValue implements DLMSObject {
	private DLMSProfileDefinition m_def[];
	private Object m_value[];
	
	// NOTE: Scaler is optional and should set to null if profile doesn't have scaler
	public DLMSProfileValue(DLMSProfileDefinition def[], InputStream is) throws IOException {
		// def must be provided
		if (def == null) throw new NullPointerException();
		m_def = def;
		if (is != null) {
			setFromStream(is);
		}
	}
	
	public void setFromStream(InputStream is) throws IOException {
		Object top = DLMSDecoderUtils.decodeStream(is);
		if (!(top instanceof Object[])) {
			throw new Error("Top level object is not array or struct");
		}
		Object[] topArr = (Object[])top;
		if (topArr.length != m_def.length) {
			throw new Error("Mismatch size");
		}
		m_value = topArr;
	}
	
	/*
	 * Formats all the records data using the following:
	 * 	{
	 *     "obis": "x.x.x.x.x.x",
	 *     "val" : number
	 *  }
	 */
	public String itemToJSON(int idx, ScalerValue scalerValue) {
		int scalerUnit = -1;
		StringBuffer sb = new StringBuffer();
		sb.append("{ ");
		sb.append("\"ic\":").append(m_def[idx].m_ic);
		sb.append(", \"obis\": \"").append(m_def[idx].obisValueToString()).append("\"");
		sb.append(", \"value\": ");
		if (m_value[idx] instanceof Calendar) {
			// TODO: In the current implementation we don't distinguish between Date/Time/DateTime
			Calendar cal = (Calendar)m_value[idx];
			sb.append('"').append(cal.get(Calendar.YEAR)).append('/').append(cal.get(Calendar.MONTH)).append('/').append(cal.get(Calendar.DAY_OF_MONTH));
			sb.append(' ').append(cal.get(Calendar.HOUR)).append(':')
					.append(cal.get(Calendar.MINUTE)).append(':')
					.append(cal.get(Calendar.SECOND)).append('.')
					.append(cal.get(Calendar.MILLISECOND)).append('"');
		} else if (m_value[idx] instanceof byte[]) {
			sb.append('"').append(DLMSDecoderUtils.bytesToHex((byte[])m_value[idx])).append('"');
		} else if (m_value[idx] instanceof Integer || m_value[idx] instanceof Long) {
			double val = ((Number)(m_value[idx])).doubleValue();

			// If scaler is provided, use it
			if (scalerValue != null) {
					Object[] scalerStruct = scalerValue.getM_value();
					if (scalerStruct.length != 2) throw new Error("Parser error: expected scaler to have 2 elements");
					Number scalerFactorObj = (Number)scalerStruct[0];
					Number scalerUnitObj   = (Number)scalerStruct[1];
					val *= Math.pow(10, scalerFactorObj.doubleValue());
					scalerUnit = scalerUnitObj.intValue();
			}
			sb.append(val);
		} else {
			sb.append(m_value[idx].toString());
		}
		// If we got information about scaler, use it to get the unit
		if (scalerUnit >= 0) {
//			sb.append(", \"scalerUnitEnum\": ").append(scalerUnit);
			sb.append(", \"scalerUnitName\": ").append(DLMSDecoderUtils.scalerUnitEnumToString(scalerUnit));
		}
		sb.append(" }");
		
		return sb.toString();
	}

	public String toJSON(String prefix, ScalerValue[] scalerValueArray) {
		StringBuffer sb = new StringBuffer();
		sb.append(prefix).append("[\n");
        for (int i = 0; i < m_def.length; ++i) {
        	sb.append(prefix).append('\t').append(itemToJSON(i,scalerValueArray[i])).append((i<m_def.length-1) ? "," : "").append('\n');
        }
		sb.append(prefix).append(']');
        return sb.toString();
	}
	
	// Retrieve the n-th element as double
	public double getValueAsDouble(int idx) {
		Object val = m_value[idx];
		if (val instanceof Number) {
			return ((Number)val).doubleValue();
		}
		throw new Error("Value is not a number");
	}
	
	public Object[] getValueAsArray(int idx) {
		Object val = m_value[idx];
		if (val instanceof Object[]) {
			return (Object[])val;
		}
		throw new Error("Value is not an array");
	}
	
	// Retrieve the n-th element as dateTime
	public Calendar getValueAsDateTime(int idx) {
		Object val = m_value[idx];
		if (val instanceof Calendar) {
			return (Calendar)val;
		}
		throw new Error("Value is not a date/time");
	}
	
	// Returns -1 if the given OBIS code does not exist in the definition of this value
	public int findPropertyIndexFromObisCode(byte [] obis) {
		for (int i = 0 ; i < m_def.length; ++i) {
			if (Arrays.equals(m_def[i].m_obis, obis)) {
				return i;
			}
		}
		return -1;
	}
}
