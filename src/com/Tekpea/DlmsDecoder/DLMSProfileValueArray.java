/*
 *  (c) Copyright, Tekpea Inc., 2014 All rights reserved.
 *
 *  No duplications, whole or partial, manual or electronic, may be made
 *  without express written permission.  Any such copies, or
 *  revisions thereof, must display this notice unaltered.
 *  This code contains trade secrets of Tekpea, Inc.
 *
 */


package com.Tekpea.DlmsDecoder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class DLMSProfileValueArray {
	private DLMSProfileDefinition[] m_def;		// The definition common to all the values 
	private Vector<DLMSProfileValue> m_values = new Vector<DLMSProfileValue>();
	
	public DLMSProfileValueArray(DLMSProfileDefinition[] def) throws IOException {
		// def & scaler must be provided
		if (def == null) {
			throw new NullPointerException();
		}
		m_def = def;
	}
	
	public void appendFromStream(InputStream is) throws IOException {
		// Expected an array of objects
		if (is.available() < 2) throw new Error("No data");
		int b = is.read();
		if (b != DLMSDecoderUtils.CosemDataType_Array) {
			throw new Error("Top level element is not an array");
		}
		// Read array size
		b = is.read();
		for (int i = 0; i < b; ++i) {
			m_values.addElement(new DLMSProfileValue(m_def, is));
		}
	}
	
	public void append(DLMSProfileValue val) {
		m_values.addElement(val);
	}
	
	public int getSize() {
		if (m_values == null) {
			return 0;
		}
		return m_values.size();
	}
	
	public DLMSProfileValue getItem(int i) {
		try {
			return m_values.elementAt(i);
		} catch(ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
}

