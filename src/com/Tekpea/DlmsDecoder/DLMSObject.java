/*
 *  (c) Copyright, Tekpea Inc., 2014 All rights reserved.
 *
 *  No duplications, whole or partial, manual or electronic, may be made
 *  without express written permission.  Any such copies, or
 *  revisions thereof, must display this notice unaltered.
 *  This code contains trade secrets of Tekpea, Inc.
 *
 */

package com.Tekpea.DlmsDecoder;

import java.io.IOException;
import java.io.InputStream;

public interface DLMSObject {

	public void setFromStream(InputStream is) throws IOException;
}
