/*
 *  (c) Copyright, Tekpea Inc., 2014 All rights reserved.
 *
 *  No duplications, whole or partial, manual or electronic, may be made
 *  without express written permission.  Any such copies, or
 *  revisions thereof, must display this notice unaltered.
 *  This code contains trade secrets of Tekpea, Inc.
 *
 */


package com.Tekpea.DlmsDecoder;

import java.io.IOException;
import java.io.InputStream;

public class DLMSProfileDefinition implements DLMSObject {
	public int m_ic    	 = 0;			// InterfaceClass
	public byte m_obis[] = {0, 0, 0, 0, 0, 0};	// OBIS Code
	public int m_ai      = 0;			// AttributeIndex
	public int m_di      = 0;			// Data Index
	
	public DLMSProfileDefinition(int classId, byte obisCode[], int attributeIndex, int dataIndex) {
		m_ic = classId;
		m_obis = obisCode;
		m_ai = attributeIndex;
		m_di = dataIndex;
	}
	
	// Builds a DLMSDefinition object from the stream 
	public DLMSProfileDefinition(InputStream is) throws IOException {
		setFromStream(is);
	}
	
	public void setFromStream(InputStream is) throws IOException {
		Object topStruct = DLMSDecoderUtils.decodeStream(is);
		
		// record is an array of fields
		if (!(topStruct instanceof Object[])) {
			throw new Error("Expected structure");
		}
		Object record[] = (Object[])topStruct;
		if (record.length != 4) {
			throw new Error("Expected 4 fields in structure, got=" + record.length);
		}
	
		// Field #1 is the class ID
		if (!(record[0] instanceof Integer)) throw new Error("Expected number for field #1");
		m_ic = ((Integer)record[0]).intValue();
		
		// Field #2 is the OBIS code
		if (!(record[1] instanceof byte[])) throw new Error("Expected buffer for field #2");
		m_obis = (byte[])record[1];
		if (m_obis.length != 6) throw new Error("Unexpected length for OBIS code=" + m_obis.length);
		
		// Field #3 is the attribute index
		if (!(record[2] instanceof Integer)) throw new Error("Expected number for field #3");
		m_ai = ((Integer)record[2]).intValue();
		
		// Field #4 is the data index
		if (!(record[3] instanceof Integer)) throw new Error("Expected number for field #4");
		m_di = ((Integer)record[3]).intValue();
	}

	public String obisValueToString() {
		StringBuffer sb = new StringBuffer();

		sb.append((int)m_obis[0] & 0xff).append('.')
				.append((int)m_obis[1] & 0xff).append('.')
				.append((int)m_obis[2] & 0xff).append('.')
				.append((int)m_obis[3] & 0xff).append('.')
				.append((int)m_obis[4] & 0xff).append('.')
                .append((int)m_obis[5] & 0xff);
		return sb.toString();
	}
	
	public String toJSON() {
		StringBuffer sb = new StringBuffer();
		sb.append("{ \"ic\": ").append(m_ic);
		sb.append(", \"obis\": \"").append(obisValueToString()).append("\"");
		sb.append(", \"ai\": ").append(m_ai);
		sb.append(", \"di\": ").append(m_di).append('}');
		return sb.toString();
	}
	
	public static DLMSProfileDefinition[] buildArrayFromStream(InputStream is) throws IOException {
		int code = is.read();
		if (code < 0) throw new Error("Not enough bytes");
		if (code != DLMSDecoderUtils.CosemDataType_Array) throw new Error("Unexpected top-level type: " + code);
		int len = DLMSDecoderUtils.decodeLength(is);
		DLMSProfileDefinition retVal[] = new DLMSProfileDefinition[len];
		for (int i = 0; i < len; ++i) {
			retVal[i] = new DLMSProfileDefinition(is);
		}
		return retVal;
	}
	
}
