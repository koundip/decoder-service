/*
 *  (c) Copyright, Tekpea Inc., 2014 All rights reserved.
 *
 *  No duplications, whole or partial, manual or electronic, may be made
 *  without express written permission.  Any such copies, or
 *  revisions thereof, must display this notice unaltered.
 *  This code contains trade secrets of Tekpea, Inc.
 *
 */

package com.Tekpea.DlmsDecoder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

// DLMS standard is big endian, all the following functions decode the integers
// as big endian.
public class DLMSDecoderUtils {
	final static int CosemDataType_Null          = 0;
	final static int CosemDataType_Array         = 1;
	final static int CosemDataType_Struct        = 2;
	final static int CosemDataType_Boolean       = 3;
	final static int CosemDataType_Bitstring     = 4;
	final static int CosemDataType_Int32         = 5;
	final static int CosemDataType_Uint32        = 6;
	final static int CosemDataType_OctetString   = 9;
	final static int CosemDataType_VisibleString = 10;
	// CosemDataType_NA            = 11;
	final static int CosemDataType_Utf8String    = 12;
	final static int CosemDataType_BCD           = 13;
	final static int CosemDataType_Int8          = 15;
	final static int CosemDataType_Int16         = 16;
	final static int CosemDataType_Uint8         = 17;
	final static int CosemDataType_Uint16        = 18;
	final static int CosemDataType_CompactArray  = 19;
	final static int CosemDataType_Int64         = 20;
	final static int CosemDataType_Uint64        = 21;
	final static int CosemDataType_Enum          = 22;
	final static int CosemDataType_Float         = 23;
	final static int CosemDataType_Double        = 24;
	final static int CosemDataType_DateTime      = 25;
	final static int CosemDataType_Date          = 26;
	final static int CosemDataType_Time          = 27;
	
	static int decodeAsInt8(InputStream is) throws IOException {
		if (is.available() < 1) throw new Error("Not enough bytes");
		byte[] buf = new byte[1];
		is.read(buf);
		return buf[0];
	}
	
	static int decodeAsUint8(InputStream is) throws IOException {
		if (is.available() < 1) throw new Error("Not enough bytes");
		return is.read();
	}
	
	static int decodeAsInt16(InputStream is) throws IOException {
		if (is.available() < 2) throw new Error("Not enough bytes");
		byte[] buf = new byte[2];
		is.read(buf);
//		return ByteBuffer.wrap(buf).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(0);
		return ((int)buf[0] << 8) | (int)buf[1];
	}
	
	static int decodeAsUint16(InputStream is) throws IOException {
		return decodeAsInt16(is);
	}
	
	static int decodeAsInt32(InputStream is) throws IOException {
		if (is.available() < 4) throw new Error("Not enough bytes");
		int retVal  = is.read() << 24;
		retVal |= is.read() << 16;
		retVal |= is.read() << 8;
		retVal |= is.read();
		return retVal;
	}
	
	static long decodeAsUint32(InputStream is) throws IOException {
		long retVal  = is.read() << 24;
		retVal |= is.read() << 16;
		retVal |= is.read() << 8;
		retVal |= is.read();
		return retVal;
	}

	static long decodeAsInt64(InputStream is) throws IOException {
		if (is.available() < 4) throw new Error("Not enough bytes");
		long retVal  = is.read() << 56;
		retVal  = is.read() << 48;
		retVal  = is.read() << 40;
		retVal  = is.read() << 32;
		retVal  = is.read() << 24;
		retVal |= is.read() << 16;
		retVal |= is.read() << 8;
		retVal |= is.read();
		return retVal;
	}
	
	static long decodeAsUint64(InputStream is) throws IOException {
		// well, this is not technically correct. Java doesn't support unsigned long
		return decodeAsInt64(is);
	}
	
	// Decodes length as BER.
	static int decodeLength(InputStream is) throws IOException {
		if (is.available() < 1) throw new Error("Not enough bytes");
		int len=is.read();
		if ((len & 0x80) != 0) {
			// Bit 7 is set
			len &= 0x7f;	// Len now represent the length of the next field
			if (len > 4) { // integers larger than 4 bytes are not supported
				throw new Error("Unsupported length format");
			}
			switch(len) {
				case 1: return decodeAsUint8(is);
				case 2: return decodeAsUint16(is);
				case 4: return (int)decodeAsUint32(is);
			}
		}
		return len;
	}
	
	static Calendar decodeAsDateTime(InputStream is, int century) throws IOException {
		int year;
		if (century > 0) {
			year = century << 8 | decodeAsUint8(is);
		} else {
			year = decodeAsUint16(is);
		}
		int month = decodeAsUint8(is);
		int dayOfMonth = decodeAsUint8(is);
		/* int dayOfWeek  = */ decodeAsUint8(is);		// Unused
		int hour = decodeAsUint8(is);
		int min = decodeAsUint8(is);
		int sec = decodeAsUint8(is);
		int centiSec  = decodeAsUint8(is);
		/* int deviation = */ decodeAsUint16(is);		// Unused
		/* int status  = */ decodeAsUint8(is);			// Unused
		Calendar retVal = Calendar.getInstance();
		retVal.set(year, month, dayOfMonth, hour, min, sec);
		retVal.set(Calendar.MILLISECOND, centiSec*10);
		return retVal;
	}
	static Calendar decodeAsDate(InputStream is) throws IOException {
		// Use the given date from stream, and set time to the beginning of the day
		int year = decodeAsUint16(is);
		int month = decodeAsUint8(is);
		int dayOfMonth = decodeAsUint8(is);
		/*int dayOfWeek  = */ decodeAsUint8(is);		// Unused
		Calendar retVal = Calendar.getInstance();
		retVal.set(year, month, dayOfMonth);
		retVal.set(Calendar.HOUR, 0);
		retVal.set(Calendar.MINUTE, 0);
		retVal.set(Calendar.SECOND, 0);
		retVal.set(Calendar.MILLISECOND, 0);
		return retVal;
	}

	static Calendar decodeAsTime(InputStream is) throws IOException {
		// Use time from stream and set today's day
		int hour = decodeAsUint8(is);
		int min = decodeAsUint8(is);
		int sec = decodeAsUint8(is);
		int centiSec  = decodeAsUint8(is);
		Calendar retVal = Calendar.getInstance();
		retVal.set(Calendar.HOUR, hour);
		retVal.set(Calendar.MINUTE, min);
		retVal.set(Calendar.SECOND, sec);
		retVal.set(Calendar.MILLISECOND, centiSec*10);
		return retVal;	
	}
	
	static Object decodeStream(InputStream is) throws IOException {
		int b;
		
		b = is.read();
		if (b < 0) {
			throw new Error("DecodeError: out of stream reached");
		}
		switch(b) {
			case CosemDataType_Null:// Null value
				return null;
				
			case CosemDataType_Boolean: // boolean
				return new Boolean(is.read() != 0);
				
			case CosemDataType_Bitstring: { // Bit-string
				int len = decodeLength(is);
				// Workaround to a bug in Genus meters that sometimes sends the length in number
				// of bits, and some other times in bytes
				if ((len % 8) == 0 && (len > 8)) {
					len /= 8;
				}
				byte buf[] = new byte[len];
				if (is.read(buf) < 0) throw new Error("Not enough bytes");
				return buf;
			}
			case CosemDataType_OctetString: {
				int len = decodeLength(is);
				if (len == 12) {
					int bb = is.read();
					if (bb == 7) {
						return decodeAsDateTime(is, 7);
					}
				}
				byte buf[] = new byte[len];
				if (is.read(buf) < 0) throw new Error("Not enough bytes");
				return buf;
			}
			
			case CosemDataType_Int32:
				return new Integer(decodeAsInt32(is));
				
			case CosemDataType_Uint32:
				return new Long(decodeAsUint32(is));

			case CosemDataType_Int16:
				return new Integer(decodeAsInt16(is));
				
			case CosemDataType_Uint16:
				return new Integer(decodeAsUint16(is));

			case CosemDataType_Int8:
				return new Integer(decodeAsInt8(is));
				
			case CosemDataType_Uint8:
				return new Integer(decodeAsUint8(is));

			case CosemDataType_Int64:
				return new Long(decodeAsInt64(is));
				
			case CosemDataType_Uint64:
				return new Long(decodeAsUint64(is));

			case CosemDataType_Struct:
			case CosemDataType_Array: {
				// Next byte is the array/struct length
				int len = decodeLength(is);
				if (is.available() < len) throw new Error("Not enough bytes");
				Object retVal[] = new Object[len];
				for (int i = 0; i < len; ++i) {
					retVal[i] = decodeStream(is);
				}
				return retVal;
			}
			
			case CosemDataType_Enum:
				// Treat enums as integer 8
				return new Integer(decodeAsUint8(is));

			case CosemDataType_CompactArray:
			case CosemDataType_BCD:
				throw new Error("Unsupported type");
				
			case CosemDataType_Date:
				return decodeAsDate(is);
			
			case CosemDataType_Time:
				return decodeAsTime(is);
			
			case CosemDataType_DateTime:
				return decodeAsDateTime(is, 0);

			case CosemDataType_Utf8String:
			case CosemDataType_VisibleString: {
				int len = decodeLength(is);
				byte data[] = new byte[len];
				if (is.read(data) < 0) throw new Error("Not enough bytes");
				return new String(data);
			}
			
			default:
				throw new Error("Unkonwn COSEM data type: " + b);
		}
	}
	
	// Just a convenience function to convert an array to string...
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
	
	public static String scalerUnitEnumToString(int val) {
		switch(val) {
			case 1: return "Year";
			case 2: return "Month";
			case 3: return "Week";
			case 4: return "Day";
			case 5: return "Hour";
			case 6: return "Minute";
			case 7: return "Second";
			case 8: return "Degree";
			// ...
			case 27: return "Watt";
			case 28: return "Volt-ampere";
			case 29: return "VAR";
			case 30: return "Watt-hour";
			case 31: return "Volt-ampere-hour";
			case 32: return "VAR-hour";
			case 33: return "Ampere";
			case 34: return "Coulomb";
			case 35: return "Volt";
			// ...
			case 44: return "Hertz";
			// ...
			default:
				return "<enum=" + Integer.toString(val) + ">";
		}		
	}
}
