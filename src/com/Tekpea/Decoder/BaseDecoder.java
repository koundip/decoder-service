package com.Tekpea.Decoder;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.Tekpea.Utils.HibernateUtils;

abstract public class BaseDecoder {

	protected SessionFactory factory;
	protected Session sess;

	public BaseDecoder() {
		// TODO Auto-generated constructor stub
		factory = HibernateUtils.getSf();
	}

	public Session getSession() {
		if(sess == null) {
			sess = factory.openSession();
		}
		return sess;
	}
}
