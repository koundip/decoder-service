package com.Tekpea.Decoder;

import com.Tekpea.models.ProfileSampleRaw;

public interface BaseDecoderInterface {
	public abstract void start() throws InterruptedException;

	public abstract void Decode(ProfileSampleRaw profileSampleRaw);
}
