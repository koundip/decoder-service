package com.Test;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.Tekpea.Utils.HibernateUtils;
import com.Tekpea.models.ProfileSampleReal;

public class ProfileSampleRealEntryAdder {

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateUtils.getSf();
		Session sess = factory.openSession();
		sess.beginTransaction();

		ProfileSampleReal sample = new ProfileSampleReal();
		sample.setProfile_param_idx(6);
		sample.setDevice_inst_idx(1);
		sample.setTs(new Date(System.currentTimeMillis()));
		sample.setValue(3.234);

		sess.save(sample);
		sess.getTransaction().commit();
		sess.close();
		factory.close();
	}

}
