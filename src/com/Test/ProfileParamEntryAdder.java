package com.Test;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.Tekpea.Utils.HibernateUtils;
import com.Tekpea.models.ProfileParam;

public class ProfileParamEntryAdder {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SessionFactory factory = HibernateUtils.getSf();
		Session sess = factory.openSession();

		sess.beginTransaction();
		ProfileParam param = new ProfileParam();
		param.setAi(2);
		param.setIc(7);
		param.setName("DLMSLoadProfile");
		param.setObis("1.2.3.4.5.6");
		sess.save(param);
		sess.getTransaction().commit();
		sess.close();
		factory.close();
	}

}
